/**
 ******************************************************************************
 * @file    main.c
 * @author  Ac6
 * @version V1.0
 * @date    01-December-2013
 * @brief   Default main function.
 ******************************************************************************
 */

#include "stm32f4xx.h"
#include "stm32f4xx_nucleo_144.h"
#include "stm32f4xx_hal_tim.h"
#include "stm32f4xx_hal_adc.h"

TIM_HandleTypeDef htim3 = { 0, };
I2C_HandleTypeDef hi2c1 = { 0, };
ADC_HandleTypeDef hadc1 = { 0, };
UART_HandleTypeDef huart3 = {0,};

//#define LCD_ADDR        (0x4E)	// PCF8574	( A2,A1,A0 = 1 )
#define LCD_ADDR		(0x7E)		// PCF8574A ( A2,A1,A0 = 1 )
#define PIN_RS         	(0b00000001)
#define PIN_EN			(0b00000100)
#define PIN_BACKLIGHT	(0b00001000)
#define LCD_DELAY_MS	5

enum state {
	DEFAULT, INITIALIZE, JOYSTICK, UART, SHOW_STATUS
};

enum state stat = DEFAULT;

// LCD에 4비트씩 쪼개서 데이터를 전송합니다.
void LCD_SendInternal(uint8_t addr, uint8_t data, uint8_t flags) {
	uint8_t up = data & 0xF0;
	uint8_t down = (data << 4) & 0xF0;
	uint8_t data_arr[4];

	data_arr[0] = up | flags | PIN_BACKLIGHT | PIN_EN; // 데이터 4비트+EN
	data_arr[1] = up | flags | PIN_BACKLIGHT;          // EN off 끊어주고,
	data_arr[2] = down | flags | PIN_BACKLIGHT | PIN_EN; // 하위 데이터 4비트 +EN
	data_arr[3] = down | flags | PIN_BACKLIGHT;          // EN off.

	HAL_I2C_Master_Transmit(&hi2c1, addr, data_arr, 4, 1000); //실제 i2c로 데이터 전송
	HAL_Delay(LCD_DELAY_MS);     //데이터 전송후 쉬어줌(LCD Datasheet 지시사항)
}

void LCD_SendCommand(uint8_t cmd) {               //RS(Register Select) 0 이면 커맨드
	LCD_SendInternal(LCD_ADDR, cmd, 0);
}

void LCD_SendData(uint8_t cmd) {
	LCD_SendInternal(LCD_ADDR, cmd, PIN_RS);  //RS(Register Select) 1 이면 데이터
}

void LCD_Init() {
	LCD_SendCommand(0b00110000); //4bit 2line 5x7 format
	LCD_SendCommand(0b00000010); //return home
	LCD_SendCommand(0b00001100); //disp on, right shift, underline off, blink off
	LCD_SendCommand(0b00000001); //
}

void clear() {
	LCD_SendInternal(LCD_ADDR, 0x01, 0);
}

void gotoxy(int x, int y) {
	LCD_SendInternal(LCD_ADDR, 0x80 | (x + y * 0x40), 0);
}

void print(char *str) {
	while (*str != 0) {
		LCD_SendInternal(LCD_ADDR, *str, PIN_RS);
		str++;
	}
}

void init() {
	HAL_Init();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	GPIO_InitTypeDef itd = { 0, };

	// LCD I2C
	itd.Pin = GPIO_PIN_8 | GPIO_PIN_9;
	itd.Mode = GPIO_MODE_AF_OD;
	itd.Pull = GPIO_PULLUP;
	itd.Alternate = GPIO_AF4_I2C1;
	itd.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(GPIOB, &itd);

	__HAL_RCC_I2C1_CLK_ENABLE();

	hi2c1.Instance = I2C1;
	hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	hi2c1.Init.ClockSpeed = 400000;

	HAL_I2C_Init(&hi2c1);

	// SERVO
	__HAL_RCC_GPIOB_CLK_ENABLE();

	itd.Pin = GPIO_PIN_0;
	itd.Mode = GPIO_MODE_AF_PP;
	itd.Alternate = GPIO_AF2_TIM3;
	HAL_GPIO_Init(GPIOB, &itd);

	__HAL_RCC_TIM3_CLK_ENABLE();

	htim3.Instance = TIM3;
	htim3.Init.Prescaler = 320;
	htim3.Init.Period = 1000; // 100 -> 2ms -> 20ms  1.5/20=0.075 * 1000 -> 75
	htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	HAL_TIM_PWM_Init(&htim3);

	TIM_OC_InitTypeDef ocinit = { 0, };
	ocinit.OCMode = TIM_OCMODE_PWM1;

	HAL_TIM_PWM_ConfigChannel(&htim3, &ocinit, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_3);

	// JOYSTICK
	__HAL_RCC_GPIOA_CLK_ENABLE();

	itd.Pin = GPIO_PIN_5 | GPIO_PIN_6;
	itd.Mode = GPIO_MODE_ANALOG;
	itd.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &itd);

	__HAL_RCC_ADC1_CLK_ENABLE();

	hadc1.Instance = ADC1;

	hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	hadc1.Init.ScanConvMode = ENABLE;
	hadc1.Init.ContinuousConvMode = DISABLE;
	hadc1.Init.DiscontinuousConvMode = ENABLE;

	hadc1.Init.NbrOfConversion = 2;         // 변환할 총 갯수
	hadc1.Init.NbrOfDiscConversion = 1;     // 한번에 변환할 갯수/ 1개씩 끊어서
	HAL_ADC_Init(&hadc1);

	ADC_ChannelConfTypeDef sConfig = { 0, };
	sConfig.Channel = ADC_CHANNEL_5;
	sConfig.Rank = 1;
	HAL_ADC_ConfigChannel(&hadc1, &sConfig);

	sConfig.Channel = ADC_CHANNEL_6;
	sConfig.Rank = 2;
	HAL_ADC_ConfigChannel(&hadc1, &sConfig);

	// STEP MOTOR
	__HAL_RCC_GPIOC_CLK_ENABLE();

	itd.Pin = GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11;
	itd.Mode = GPIO_MODE_OUTPUT_PP;
	itd.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(GPIOC, &itd);

	// UART
    __HAL_RCC_GPIOD_CLK_ENABLE();

    itd.Pin = GPIO_PIN_8;
    itd.Mode = GPIO_MODE_AF_PP;
    itd.Alternate = GPIO_AF7_USART3;
    itd.Speed = GPIO_SPEED_HIGH;
    itd.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOD, &itd);

    itd.Pin = GPIO_PIN_9;
    itd.Mode = GPIO_MODE_AF_OD;
    HAL_GPIO_Init(GPIOD, &itd);

    huart3.Instance = USART3;
    huart3.Init.BaudRate = 115200;
    huart3.Init.Mode = UART_MODE_TX_RX;

    __HAL_RCC_USART3_CLK_ENABLE();
    HAL_UART_Init(&huart3);
    // 115200-N-8-1

    HAL_NVIC_EnableIRQ(USART3_IRQn);
}

void USART3_IRQHandler(void)
{
        HAL_UART_IRQHandler(&huart3);
}

char buf[100];
char recv[100];
int buf_pos = 0;
int buf_recv_line = 0;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
        if (buf[buf_pos] == '\n')
        {
                buf[buf_pos] = 0;
                strcpy(recv, buf);
                buf_pos = 0;
                buf_recv_line = 1;
        } else {
                buf_pos++;
        }
        HAL_UART_Receive_IT(&huart3, buf+buf_pos, 1); // 인터럽트 1회 작동 이후에는 작동하지 않기 때문에 다시 값을 받기 위해서
}

int StepTable[] = { 0b0001, 0b0011, 0b0010, 0b0110, 0b0100, 0b1100, 0b1000,
		0b1001 };

volatile int i = 0;
volatile int step_cnt = 0;
volatile int step_speed = 0;		// 최대 100
volatile int step_max_speed = 100;
volatile int rotation = 0;			// 0 : cw,  1 : ccw

volatile int survo_control = 0;		// 최대 50
volatile int survo_dir = 0;			// 0 : left,  1 : right

volatile int tick = 0;				// hour, minute, second

int func = 0;						// 스탭모터 최대속도 동작
int consume_fuel = 0;				// 속도에 따라 초당 소모량 변경
int total_consume_fuel = 0;

void HAL_SYSTICK_Callback() {
	if((HAL_GetTick() % 1000) == 0){		// 1s : == 0 뺴먹지말것!!!
		if(step_speed >= 50){
			consume_fuel = 3;
		}else if((0 < step_speed) && (step_speed < 50)){
			consume_fuel = 1;
		}else{
			consume_fuel = 0;
		}

		total_consume_fuel += consume_fuel;

		// second(60), minute(60), hour(24)
		tick = (tick == 86400)? 0 : tick+1;
	}

	if (stat == JOYSTICK || stat == SHOW_STATUS) {
		// 스탭모터 속도 / 방향제어
		HAL_GPIO_WritePin(GPIOC, 0b111100000000, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOC, StepTable[i] << 8, GPIO_PIN_SET);

		step_cnt = step_cnt + step_speed;
		if (step_cnt >= step_max_speed) // overflow!
		{
			step_cnt = step_cnt % step_max_speed;

			if (rotation == 0) {
				i = (i + 1) % 8;
			} else {
				i = (i == 0) ? 7 : i - 1;
			}
		}

		// 서보모터 조향
		if (survo_dir == 0) {
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, 75 + survo_control);
		} else {
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, 75 - survo_control);
		}
	}else if (stat == UART) {
		if(buf_recv_line){
			if(recv[0] == '5'){
				func = 0;
				HAL_UART_Transmit(&huart3, "Input Control 1) Remote Control 2) Show Status \r\n", 49, 1000);
				stat = JOYSTICK;
			}else if(recv[0] == '1'){
				func = 1;		// 스텝모터를 최대속도로 동작
			}else if(recv[0] == '2'){
				func = 0;		// 스탭모터를 정지
			}else if(recv[0] == '3'){
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, 125);		// 서보를 좌축으로 고정
			}else if(recv[0] == '4'){
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, 25);		// 서보를 우측으로 고정
			}

			buf_recv_line = 0;
		}
	}

	if(func){
		HAL_GPIO_WritePin(GPIOC, 0b111100000000, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOC, StepTable[i] << 8, GPIO_PIN_SET);

		step_cnt = step_cnt + 100;		// step_speed = 100
		if (step_cnt >= step_max_speed) // overflow!
			{
			step_cnt = step_cnt % step_max_speed;

			if (rotation == 0) {		// 기존방향 그대로 사용
				i = (i + 1) % 8;
			} else {
				i = (i == 0) ? 7 : i - 1;
			}
		}
	}
}

char string[20];

int main(void) {
	init();
	LCD_Init();

// a) 부팅시 다음 두 화면이 1초씩 번갈아가며 총 3회 표시되게 하라.
	//	stat = INITIALIZE;
	for (int i = 0; i < 3; i++) {
		print("--CAR CONTROL--");
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, 25);
		HAL_Delay(1000);
		clear();
		print(" Initialize .. ");
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, 125);
		HAL_Delay(1000);
		clear();
	}
	sprintf(string, "  %3dkm/h  %3dL     ", step_speed, total_consume_fuel);
	gotoxy(0, 0);
	print(string);
	gotoxy(0, 1);
	sprintf(string, "  %2d:%2d:%2d      ", tick/3600, (tick%3600)/60, (tick%3600)%60);
	print(string);

// b) 아날로그 스틱의 입력을 받게 하여 다음 동작을 수행할 것
// c) UART 연결시 다음 동작을 수행할 것
	stat = JOYSTICK;

	HAL_UART_Transmit(&huart3, "Input Control 1) Remote Control 2) Show Status \r\n", 49, 1000);
	HAL_UART_Receive_IT(&huart3, buf, 1);
	for (;;) {
		volatile unsigned int v1, v2;
		// UART 명령체크
		if(buf_recv_line){
			if(recv[0] == '1'){
				buf_recv_line = 0;
				stat = UART;
				HAL_UART_Transmit(&huart3, "Remote Control 1) Full Speed 2) Stop 3) Left 4) Right 5) Exit\r\n", 63, 1000);
				while(stat == UART){
					clear();
					HAL_Delay(500);
					print("               R");
					HAL_Delay(500);
				}
			}else if(recv[0] == '2'){
				stat = SHOW_STATUS;
			}else{
				// DO NOTHING
			}
			buf_recv_line = 0;
		}

		// 스탭모터 속도/방향 조절
		HAL_ADC_Start(&hadc1);
		int ret1 = HAL_ADC_PollForConversion(&hadc1, 1000);
		v1 = HAL_ADC_GetValue(&hadc1);// 2090 기준, 한방향 최대 2048 ADC, 100 step_speed

		if (v1 < 1990) {
			rotation = 0;
			step_speed = (int) (((1990 - v1) * 100 / 2048.0));
		} else if (v1 > 2190) {
			rotation = 1;
			step_speed = (int) (((v1 - 2190) * 100 / 2048.0));
		} else {
			// 중립
			step_speed = 0;
		}

		// 서보모터 조향
		HAL_ADC_Start(&hadc1);
		int ret2 = HAL_ADC_PollForConversion(&hadc1, 1000);
		v2 = HAL_ADC_GetValue(&hadc1);// 2050 기준, 한방향 최대 2048 ADC, 50 survo_control

		if (v2 < 1950) {
			survo_dir = 0;
			survo_control = (int) (((1950 - v2) * 50 / 2048.0));
		} else if (v2 > 2150) {
			survo_dir = 1;
			survo_control = (int) (((v2 - 1950) * 50 / 2048.0));
		} else {
			// 중립
			survo_control = 0;
		}
		HAL_ADC_Stop(&hadc1);

		if(stat == SHOW_STATUS){
			gotoxy(0, 0);
			sprintf(string, "Speed: %3dkmh  ", step_speed);
			print(string);
			gotoxy(0, 1);
			sprintf(string, "Steer:%s Gas:%3d", (survo_dir==0)?"L":"R",total_consume_fuel);
			print(string);
		}else if(stat == JOYSTICK){
			sprintf(string, "  %3dkm/h  %3dL     ", step_speed, total_consume_fuel);
			gotoxy(0, 0);
			print(string);
			gotoxy(0, 1);
			sprintf(string, "  %2d:%2d:%2d      ", tick/3600, (tick%3600)/60, (tick%3600)%60);
			print(string);
		}else{
			// DO NOTHING
		}
	}
}

